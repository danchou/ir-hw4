'''
Created on Apr 3, 2015

@author: zhoupeng
'''
import unittest
from trec_eval import *

class Test(unittest.TestCase):
    
    def setUp(self):
        self.qrelmap = loadQrel('data/qrels.adhoc.51-100.AP89.txt')
        self.evalmap = loadEval('data/uuuu')

    def test_loadQrel(self):
        print self.qrelmap['61']

    def test_eval(self):
        qid = '61'
        lst = []
        labels = []
        for e in self.evalmap[qid]:
            try:
#                 print e[0], e[1]
                labels.append(self.qrelmap[qid][e[0]])
            except:
                labels.append(0.0)
        
        trelcount = reduce(lambda acc, e: acc+(1 if e!=0 else 0),
                            self.qrelmap[qid].itervalues(), 0)
        ret = evalQuery(labels, trelcount)
        displayQuery(qid, ret)

if __name__ == "__main__":
    unittest.main()