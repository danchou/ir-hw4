'''
Created on Apr 5, 2015

@author: zhoupeng
'''
import sys
import numpy as np
import scipy.sparse as ss

DEBUG = 2

DAMPING = 0.8
MAX_ITER = 100
THRESHOLD = 1e-16

class Graph:
    
    def __init__(self, mappername, outlinkname):
        mapper = self.loadmapper(mappername)
        self.mapper = mapper[0]
        self.revmapper = mapper[1]
        self.outlinkmap = self.loadOutlinks(outlinkname)

    def loadmapper(self, fname):
        cache = {}
        revcache = []
        with open(fname) as f:
            i = 0
            for line in f:
                pageid = line.strip()
                revcache.append(pageid)
                cache[pageid] = i
                i += 1
        return cache, revcache

    def loadOutlinks(self, fname):
        cache = {}
        with open(fname) as f:
            for line in f:
                lst = line.strip().split()
                cache[self.mapper[lst[0]]] = [self.mapper[pid] for pid in lst[1:]]
        return cache

def buildTransitionMatrix(outlinkmap, n):
    '''
    @param outlinkmap: a outlink dictionary
    @param n: number of nodes
    @return: a csc_matrix
    '''
    print '-- Building %d * %d matrix --'%(n,n)
    matrix = ss.lil_matrix((n+1,n+1))

    for key, values in outlinkmap.iteritems():
        N = len(values)
        for v in values: matrix[key,v] += 1.0 / N * DAMPING

    # add last row and last column for damping factor
    for i in range(n):
        matrix[n,i] = (1 - DAMPING) / n
        matrix[i,n] = 0
    matrix[n,n] = 1
    return matrix.tocsc()

def buildPRVector(n):
    vec = np.full((1,n+1), 1.0/n)
    vec[0, n] = 1
    return vec

def iterate(graph):
    outlinkmap = graph.outlinkmap
    n = len(outlinkmap)
    matrix = buildTransitionMatrix(outlinkmap, n)
    pr_vector = buildPRVector(n)

    print '-- Start Iterating --'
    delta = 1
    i = 0
    while delta > THRESHOLD and i < MAX_ITER:
        new_pr_vector = pr_vector * matrix
        delta = np.linalg.norm(new_pr_vector-pr_vector)
        pr_vector = new_pr_vector
#         print 'iteration %d: delta:%.9f'%(i, delta)
        i += 1
    return pr_vector

def sort(page_rank_score, mapping):
    n = len(page_rank_score)
    for i in sorted(range(n), key=lambda e: page_rank_score[e]):
        print mapping[i], page_rank_score[i]

def main():
    if DEBUG==0:
        graph = Graph('data/small_mapping', 'data/small_outlinks.txt')
    elif DEBUG==1:
        graph = Graph('data/wt2g_mapping', 'data/wt2g_outlinks.txt')
    else:
        graph = Graph('data/my_mapping', 'data/my_outlinks.txt')
    res = iterate(graph)[0,0:-1]
    sort(res, graph.revmapper)

if __name__ == '__main__':
    main()