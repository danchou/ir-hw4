#!/usr/bin/python

import argparse
import math

K = [5, 10, 20, 50 ,100]

def loadQrel(fqrelname):
    cache = {}
    with open(fqrelname) as f:
        for line in f:
            lst = line.strip().split()
            try:
                cache[lst[0]][lst[2]] = float(lst[3])
            except:
                cache[lst[0]] = {}
                cache[lst[0]][lst[2]] = float(lst[3])
                
    return cache

def loadEval(fevalname):
    cache = {}
    with open(fevalname) as f:
        for line in f:
            lst = line.strip().split()
            try:
                cache[lst[0]].append((lst[2], float(lst[4])))
            except:
                cache[lst[0]] = [(lst[2], float(lst[4]))]
    for value in cache.itervalues(): value.sort(key=lambda i: i[1], reverse=True)
    return cache

def dcg(labels):
    assert len(labels) > 0
    s = labels[0]
    for i in range(1, len(labels)):
        s += labels[i] / math.log(i+1, 2)
    return s
    
def evalQuery(labels, trelcount):
    '''
    @param lst: a list of docid already sorted by score.
    '''
    n = len(labels)
    precisions = [0.0] * n
    recalls = [0.0] * n
    fmeasure = [0.0] * n
    
    precisionSum = 0.0
    precisionSumCount = 0
    
    scoreToNow = 0.0
    for i in range(n):
        scoreToNow +=  labels[i]
        precisions[i] = scoreToNow / (i+1)
        recalls[i] = scoreToNow / trelcount

        if precisions[i] + recalls[i]!=0:
            fmeasure[i] = 2 * precisions[i] * recalls[i] / (precisions[i] + recalls[i])
        
        if labels[i]!=0 :
            precisionSumCount += 1
            precisionSum += precisions[i]
    
    if precisionSum==0: avgPrecision = 0
    else: avgPrecision = precisionSum / trelcount

    if trelcount>n:
        RPrecision = scoreToNow / trelcount
    else:
        RPrecision = precisions[trelcount-1]

    d = dcg(sorted(labels, reverse=True))
    nDCG = dcg(labels) / d if d else 0
    
    return avgPrecision, RPrecision, nDCG,\
        [precisions[i-1] for i in K], [recalls[i-1] for i in K], \
        [fmeasure[i-1] for i in K]


def trec_eval(fqrelname, fevalname):
    qrelmap = loadQrel(fqrelname)
    evalmap = loadEval(fevalname)
    
    rets = {}
    for qid, rank in evalmap.iteritems():
        labels = []
        for e in rank:
            try:
                labels.append(qrelmap[qid][e[0]])
            except:
                labels.append(0.0)
        trelcount = reduce(lambda acc, e: acc+(1 if e!=0 else 0),
                            qrelmap[qid].itervalues(), 0)
        rets[qid] = evalQuery(labels, trelcount)
    return rets

def displayAvg(rets):
    n = len(rets)
    if n==0: return

    sum_avg_precision = 0
    sum_rprecision = 0
    sum_ndcg = 0

    sum_precisions = [0.0] * len(K)
    sum_recalls = [0.0] * len(K)
    sum_fmeasure = [0.0] * len(K)

    for value in rets.itervalues():
        sum_avg_precision += value[0]
        sum_rprecision += value[1]
        sum_ndcg += value[2]
    
        for i in range(len(K)):
            sum_precisions[i] += value[3][i]
            sum_recalls[i] += value[4][i]
            sum_fmeasure[i] += value[5][i]

    print 'Average over all queries.'    
    print 'Precision: ', sum_avg_precision / n
    print 'RPrecision: ', sum_rprecision / n
    print 'nDCG: ', sum_ndcg / n
    
    print 'R\t\t', 'Precision\t', 'Recall\t\t', 'FMeasure'
    for i in range(len(K)):
        print 'document at {0:d}\t{1:f}\t{2:f}\t{3:f}'.\
            format(K[i], sum_precisions[i]/n, sum_recalls[i]/n, sum_fmeasure[i]/n)

def displayQuery(key, ret):
    print 'query: ', key
    print 'Precision: ', ret[0]
    print 'RPrecision: ', ret[1]
    print 'nDCG: ', ret[2]
    print 'R\t\t', 'Precision\t', 'Recall\t\t', 'FMeasure'
    for i in range(len(K)):
        print 'document at {0:d}\t{1:f}\t{2:f}\t{3:f}'.\
            format(K[i], ret[3][i], ret[4][i], ret[5][i])
    print '\n'

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('qrel')
    parser.add_argument('eval')
    parser.add_argument('-queryall', '-q', action='store_true')
    param = parser.parse_args()

    fqrelname = param.qrel
    fevalname = param.eval

    rets = trec_eval(fqrelname, fevalname)
    if param.queryall:
        for key, value in rets.iteritems():
            displayQuery(key, value)
    else:
        displayAvg(rets)


def test_trec_eval():
    rets = trec_eval('data/qrels.adhoc.51-100.AP89.txt', 'data/uuuu')
    for key, value in rets.iteritems():
        displayQuery(key, value)
    displayAvg(rets)


if __name__ == '__main__':
    main()