'''
Created on Apr 5, 2015

@author: zhoupeng
'''
import unittest
import numpy.testing as nptest

from page_rank import *

class Test(unittest.TestCase):

    def setUp(self):
        unittest.TestCase.setUp(self)
        self.outlinkmap = {0:[1, 2], 1:[0, 1, 2], 2:[0, 2]}
        self.n = 3

    def tearDown(self):
        unittest.TestCase.tearDown(self)

    def test_buildTransitionMatrix(self):
        m = buildTransitionMatrix(self.outlinkmap, self.n)
        nptest.assert_almost_equal(m[0,0], 0.0 * DAMPING)
        nptest.assert_almost_equal(m[0,1], 0.5 * DAMPING)
        nptest.assert_almost_equal(m[0,2], 0.5 * DAMPING)
        nptest.assert_almost_equal(m[0,3], 0.0 * DAMPING)
        
        nptest.assert_almost_equal(m[1,0], 1.0/3 * DAMPING)
        nptest.assert_almost_equal(m[1,1], 1.0/3 * DAMPING)
        nptest.assert_almost_equal(m[1,2], 1.0/3 * DAMPING)
        nptest.assert_almost_equal(m[1,3], 0.0 * DAMPING)
        
        nptest.assert_almost_equal(m[2,0], 0.5 * DAMPING)
        nptest.assert_almost_equal(m[2,1], 0.0 * DAMPING)
        nptest.assert_almost_equal(m[2,2], 0.5 * DAMPING)
        nptest.assert_almost_equal(m[2,3], 0.0 * DAMPING)
        
        nptest.assert_almost_equal(m[3,0], (1.0-DAMPING)/3)
        nptest.assert_almost_equal(m[3,1], (1.0-DAMPING)/3)
        nptest.assert_almost_equal(m[3,2], (1.0-DAMPING)/3)
        nptest.assert_almost_equal(m[3,3], 1.0)
    
    def test_buildPRVector(self):
        v = buildPRVector(10)
        nptest.assert_almost_equal(v[0,0], 1.0/10)
        nptest.assert_almost_equal(v[0,0], 1.0/10)
        nptest.assert_almost_equal(v[0,10], 1.0)
        
    def test_multiply(self):
        m = buildTransitionMatrix(self.outlinkmap, self.n)
        v = buildPRVector(self.n)
        r = v * m
        nptest.assert_almost_equal(np.sum(r), 2.0)
        r = v * m
        nptest.assert_almost_equal(np.sum(r), 2.0)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()