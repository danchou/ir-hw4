'''
Created on Apr 5, 2015
@author: zhoupeng
'''
import sys

'''
usage [outlinksfile]
'''

def main():
    fname = sys.argv[1]
    with open(fname) as f:
        for line in f:
            idx = nodeInLine(line)
            if idx: print idx

def nodeInLine(line):
    i = 0
    n = len(line)
    while i<n and line[i]!=' ': i += 1
    return line[0:i]

if __name__ == '__main__':
    main()