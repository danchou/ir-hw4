'''
Created on Apr 5, 2015

@author: zhoupeng
'''
import sys
from pprint import pprint

def main():
    cache = {}
    with open(sys.argv[1]) as f:
        for line in f:
            lst = line.strip().split()
            node = lst[0]
            if node not in cache:
                cache[node] = []

            for inlink in lst[1:]:
                try:
                    cache[inlink].append(node)
                except:
                    cache[inlink] = []
                    cache[inlink].append(node)
    
    for key in sorted(cache.iterkeys()):
        print key + ' ' +' '.join(sorted(cache[key]))

if __name__ == '__main__':
    main()