import shelve

db = shelve.open('Score.db')

lst = []
for key, value in db.iteritems():
    pair = str(value).split(',')
    try:
        lst.append((key, int(pair[0]), int(pair[1])))
    except:
        continue

lst.sort(key=lambda x : x[2])
for i in lst: print i
