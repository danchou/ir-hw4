import shelve

class ShelveStorage():

    def __init__(self, dbpath):
        self.db = shelve.open(dbpath)

    def put(self, key ,data):
        self.db[key] = data

    def get(self, key):
        return self.db[key]

def test():
    s = ShelveStorage('test.db')
    s.put('hello', 'good')
    print s.get('hello')

if __name__ == '__main__':
    test()
