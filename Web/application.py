from flask import Flask
from flask import render_template
from flask import request
from AccessControl import *
from Storage import *
import sys

app = Flask(__name__)
app.debug = True
storage = ShelveStorage('Score.db')
accessid = sys.argv[1]

@app.route('/')
def index():
    return 'hello world'

@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
    return render_template('index.html', name=name)

@app.route('/test')
def test():
    return render_template('test.html')

@app.route('/score_page', methods=['GET'])
@crossdomain(origin='*')
def scorePage():
    pageid = request.args.get('pageid')
    score = request.args.get('score')
    rank = request.args.get('rank')
    
    if not pageid or not score:
        print>>sys.stderr, 'invalid request'
        return '0'

    print 'evaluates doc:%s score:%s rank:%s'%(pageid, score, rank)
    storage.put(str(pageid), score+','+ rank)

    return '1'

if __name__ == '__main__':
    app.run()
