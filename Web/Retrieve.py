import shelve
import sys

db = shelve.open(sys.argv[1])

lst = []
for e in db.items():
    pair = str(e[1]).split(',')
    lst.append((e[0], int(pair[0]), int(pair[1])))

lst.sort(key=lambda x: x[2])
for e in lst: print e[0], e[1], e[2]
