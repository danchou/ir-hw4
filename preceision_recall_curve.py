import sys
import matplotlib.pyplot as plt

DEBUG = 1

def loadScore(fname):
    scores = []
    with open(fname) as f:
        for line in f:
            s = int(line.split()[1])
            scores.append(1 if s else 0)
    return scores

def PrecisionRecall(scores):
    rel = len(filter(lambda x: x!=0, scores))
    pr = []
    cur_score = 0.0
    for i in range(len(scores)):
        cur_score += scores[i]
        pr.append((cur_score/(i+1), cur_score/rel))
    return pr

def forceNonincrease(xx, yy):
    assert len(xx) == len(yy)
    n = len(xx)

    lst = list(yy)
    cur_max = -sys.maxint
    for i in range(n-1, -1, -1):
        if lst[i] > cur_max: 
            cur_max = yy[i]
        else:
            lst[i] = cur_max
    return lst

def plotCurve(pr):
    pr.sort(key=lambda x : x[1])
    yy, xx = zip(*pr)
    yyy = forceNonincrease(xx, yy)

    plt.plot(xx, yy, 'b')
    plt.plot(xx, yyy, 'r')
    plt.show()
    
def main():
    if DEBUG:
        sys.argv.append('data/evaluation/RecentPope')
        scores = loadScore(sys.argv[1])
        pr = PrecisionRecall(scores)
        plotCurve(pr)
    
if __name__ == '__main__':
    main()